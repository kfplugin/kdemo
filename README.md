# kdemo

> 基于金堂的kdemo来的

提供给演示kubernetes集群部署实际项目实践使用

包括：

- 基于yii2构建的基础项目，做了针对配置文件获取环境变量的改动
- Dockerfile打包构建镜像

## 环境依赖

- composer
- docker
- make

## 使用指南

拉取代码

```bash
git clone git@git.mysre.cn:helong/kdemo.git
```

进入kdemo目录

```bash
cd kdemo
```

打包镜像

```bash
make build
```

运行

```bash
make run
```