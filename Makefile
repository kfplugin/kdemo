.PHONY: build
build:
	docker build -t kdemo:$(tag) .

.PHONY: run
run:
	docker ps -a | grep kdemo | awk '{print $1}'| xargs -I{} docker rm {}
	docker run --name kdemo -d -p $(port):80 --link=mysql57 kdemo:$(tag)