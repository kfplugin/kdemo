FROM php:7.2-fpm-alpine3.12

# PHP_CPPFLAGS are used by the docker-php-ext-* scripts
# ENV PHP_CPPFLAGS="$PHP_CPPFLAGS -std=c++11"

RUN sed -i 's/dl-cdn.alpinelinux.org/mirrors.aliyun.com/g' /etc/apk/repositories

RUN apk update \
    && apk add --no-cache nginx icu-dev freetype-dev libjpeg-turbo-dev libpng-dev \
    && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install -j$(nproc) gd \
    && docker-php-ext-install pdo_mysql \
    && docker-php-ext-install opcache \
    && docker-php-ext-configure intl \
    && docker-php-ext-install intl \
    && { \
        echo 'opcache.memory_consumption=128'; \
        echo 'opcache.interned_strings_buffer=8'; \
        echo 'opcache.max_accelerated_files=4000'; \
        echo 'opcache.revalidate_freq=2'; \
        echo 'opcache.fast_shutdown=1'; \
        echo 'opcache.enable_cli=1'; \
    } > /usr/local/etc/php/conf.d/php-opocache-cfg.ini

COPY kdemo.conf /etc/nginx/conf.d/default.conf
COPY entrypoint.sh /etc/entrypoint.sh
ADD --chown=www-data:www-data src /webser/www/kdemo

WORKDIR /webser/www/kdemo

EXPOSE 80 443

RUN chmod +x /etc/entrypoint.sh && mkdir -p /run/nginx

ENTRYPOINT ["/etc/entrypoint.sh"]
