<?php

function get_env($key, $default = null)
{
    return !empty(getenv($key)) ? getenv($key) : $default;
}

$host = get_env('MYSQL_HOST', 'mysql57');
$dbName = get_env('MYSQL_DB_NAME','basic');
$user = get_env('MYSQL_DB_USER', 'root');
$password = get_env('MYSQL_DB_PASSWORD', 'password');

return [
    'class' => 'yii\db\Connection',
    'dsn' => "mysql:host={$host};dbname={$dbName}",
    'username' => $user,
    'password' => $password,
    'charset' => 'utf8',

    // Schema cache options (for production environment)
    //'enableSchemaCache' => true,
    //'schemaCacheDuration' => 60,
    //'schemaCache' => 'cache',
];
