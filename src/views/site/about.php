<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (count($students) > 0): ?>
    <table width="100%" border="1">
        <thead>
            <tr><th>id</th><th>名称</th><th>学号</th></tr>
        </thead>
        <body>
    <?php foreach($students as $stu): ?>
        <tr><td><?= $stu['id'] ?></td><td><?= $stu['name'] ?></td><td><?= $stu['student_no'] ?></td></tr>
    <?php endforeach; ?>
        </body>
    </table>
    <?php endif; ?>
</div>
