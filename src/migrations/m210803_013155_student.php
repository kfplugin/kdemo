<?php

use yii\db\Migration;

/**
 * Class m210803_013155_student
 */
class m210803_013155_student extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable("student", [
            'id' => $this->primaryKey(10)->unsigned()->comment('id'),
            'name' => $this->string(128)->notNull()->defaultValue('')->comment('姓名'),
            'student_no' => $this->string(11)->notNull()->defaultValue('')->comment('学号'),
        ]);
        $this->createIndex('student_no', 'student', ['student_no'], true);

        $this->insert('student', ['name' => 'blake', 'student_no' => '10000000000']);
        $this->insert('student', ['name' => 'jay', 'student_no' => '10000000001']);
        $this->insert('student', ['name' => 'alice', 'student_no' => '10000000002']);
        $this->insert('student', ['name' => 'bob', 'student_no' => '10000000003']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('student');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210803_013155_student cannot be reverted.\n";

        return false;
    }
    */
}
